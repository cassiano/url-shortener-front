import axios from 'axios'
import { ShortUrlType, ShortUrlsStatsType } from '../App'

axios.defaults.baseURL = 'http://localhost:3001/api'
axios.defaults.withCredentials = true

export const listShortUrlsApi = () => axios.get<ShortUrlType[]>('/short_urls')
export const deleteShortUrlApi = (id: number) =>
  axios.delete(`/short_urls/${id}`)
export const createShortUrlApi = (url: string) =>
  axios.post<ShortUrlType>('/short_urls', { url })
export const showShortUrlApi = (id: number) =>
  axios.get<ShortUrlType>(`/short_urls/${id}`)
export const getShortUrlsStatsApi = () =>
  axios.get<ShortUrlsStatsType>('/short_urls/stats')
