import { useEffect, useState } from 'react'
import { AxiosResponse } from 'axios'

type genericLoaderOptions = { deps?: unknown[] | null; timeout?: number }

export const useGenericLoader = <T>(
  initialValue: T,
  apiFn: () => Promise<AxiosResponse<T, any>>,
  { deps = [], timeout = undefined }: genericLoaderOptions = {}
) => {
  const [state, setState] = useState<T>(initialValue)

  useEffect(
    () => {
      const apiFnCall = () => {
        apiFn()
          .then(response => setState(response.data))
          .catch(console.log)
      }

      if (timeout !== undefined) setTimeout(apiFnCall, timeout)
      else apiFnCall()
    },
    deps !== null ? deps : undefined
  )

  return [state, setState] as const
}
