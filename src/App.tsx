import './App.css'

import { useState } from 'react'
import { useGenericLoader } from './hooks/loaders'
import { showShortUrlApi } from './services/api'
import {
  listShortUrlsApi,
  createShortUrlApi,
  deleteShortUrlApi,
  getShortUrlsStatsApi,
} from './services/api'

export type ShortUrlType = {
  id: number
  uniqueId: string
  links: {
    visit: string
  }
  visited: number
}

export type ShortUrlsStatsType = {
  shortUrls: {
    generated: number
    visited: number
  }
}

const copyUrlToClipboard = (url: string) => {
  if (navigator?.clipboard?.writeText) {
    navigator.clipboard
      .writeText(url)
      .then(() => console.log(`URL ${url} copied to clipboard.`))
  }
}

type AppProps = {}

export const App: React.FC<AppProps> = () => {
  const [url, setUrl] = useState('')
  const [changesCount, setChangesCount] = useState(0)

  const [shortUrls, setShortUrls] = useGenericLoader([], listShortUrlsApi)
  const [shortUrlStats] = useGenericLoader(
    {
      shortUrls: { generated: 0, visited: 0 },
    },
    getShortUrlsStatsApi,
    { deps: [changesCount], timeout: 500 }
  )

  const handleUrlShortenClick = (
    _event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    createShortUrlApi(url)
      .then(({ data: newLink }) => {
        setShortUrls(previousLinks => [newLink, ...previousLinks])
        setUrl('')
        copyUrlToClipboard(newLink.links.visit)
        incrementDataUpdatesCount()
      })
      .catch(err => {
        if (err.response.statusText === 'Unprocessable Entity')
          alert(err.response.data.join('\n'))
        else console.log(err)
      })
  }

  const handleDeleteShortUrlClick =
    (deletedLink: ShortUrlType) =>
    (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      event.preventDefault()

      if (
        window.confirm(
          `Are you sure you want to delete short URL '${deletedLink.uniqueId}'?`
        )
      )
        deleteShortUrlApi(deletedLink.id)
          .then(() => {
            setShortUrls(previousLinks =>
              previousLinks.filter(link => link.id !== deletedLink.id)
            )

            incrementDataUpdatesCount()
          })
          .catch(console.log)
    }

  const handleCopyUrlToClipboardClick =
    (link: ShortUrlType) => (event: React.MouseEvent<HTMLElement>) => {
      copyUrlToClipboard(link.links.visit)
    }

  const handleShortUrlVisitClick =
    (visitedShortUrl: ShortUrlType) =>
    (_event: React.MouseEvent<HTMLElement>) => {
      setTimeout(() => {
        incrementDataUpdatesCount()

        showShortUrlApi(visitedShortUrl.id)
          .then(response =>
            setShortUrls(previousShortUrls =>
              previousShortUrls.map(shortUrl =>
                shortUrl.id === visitedShortUrl.id ? response.data : shortUrl
              )
            )
          )
          .catch(console.log)
      }, 500)
    }

  const incrementDataUpdatesCount = () => {
    setChangesCount(previousCount => previousCount + 1)
  }

  return (
    <div className='App'>
      <fieldset>
        <legend>URL Shortner</legend>
        <textarea
          placeholder='URL to shorten'
          cols={80}
          rows={5}
          value={url}
          onChange={e => setUrl(e.target.value)}
        />
        <br />
        <button
          type='button'
          onClick={handleUrlShortenClick}
          disabled={url === ''}
        >
          Shorten
        </button>
      </fieldset>
      <p>
        Short URLs generated: {shortUrlStats.shortUrls.generated} | Visited:{' '}
        {shortUrlStats.shortUrls.visited}
      </p>
      <ol>
        {shortUrls.map(shortUrl => (
          <li key={shortUrl.id}>
            <a
              href={shortUrl.links.visit}
              target='_blank'
              rel='noreferrer'
              onClick={handleShortUrlVisitClick(shortUrl)}
            >
              {shortUrl.uniqueId}
            </a>
            &nbsp; - {shortUrl.visited} Visit{shortUrl.visited !== 1 && 's'}{' '}
            &nbsp;
            <a href='#' onClick={handleDeleteShortUrlClick(shortUrl)}>
              [delete]
            </a>
            &nbsp;
            <a href='#' onClick={handleCopyUrlToClipboardClick(shortUrl)}>
              [copy]
            </a>
          </li>
        ))}
      </ol>
    </div>
  )
}

export default App
